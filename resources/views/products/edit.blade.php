@extends('adminlte::page')

@section('title', 'Edit Product')

@section('content_header')
    <h1 class="m-0 text-dark">Edit Product</h1>
@stop

@section('content')
    <form action="{{route('products.update',$product)}}" method="post">
        @method('PUT')
        @csrf
        <input type="hidden" name="action" value="update_info">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" name="categories_id">
                            <option value="" disabled selected>Select a Category</option>
                            @foreach ($categories as $_cat)
                                <option value="{{ $_cat->id }}" {{ ($product->categories_id == $_cat->id) ? 'selected' : ''  }}>{{ $_cat->name }}</option>
                            @endforeach
                        </select>
                        @error('categories_id') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="" name="name" value="{{ $product->name }}">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>Desc</label>
                        <textarea name="desc" class="form-control @error('desc') is-invalid @enderror" >{{ $product->desc }}</textarea>
                        @error('desc') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>Photo(s)</label>
                        <br>
                        <small><i>*Each URL Seperate by New Line</i></small>
                        <textarea name="photos" class="form-control @error('photos') is-invalid @enderror" rows="5" id="inputPhotos"></textarea>
                        @error('photos') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>Color(s)</label>
                        <select class="form-control" name="colors_id[]" size="{{ count($colors)+1 }}" multiple="multiple" id="selectColors">
                            <option value="" disabled>Select one or many color</option>
                            @foreach ($colors as $_col)
                                <option value="{{ $_col->id }}">{{ $_col->name }}</option>
                            @endforeach
                        </select>
                        @error('colors_id') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>Size(s)</label>
                        <select class="form-control" name="sizes_id[]" size="{{ count($sizes)+1 }}" multiple="multiple" id="selectSizes">
                            <option value="" disabled>Select one or many Size</option>
                            @foreach ($sizes as $_size)
                                <option value="{{ $_size->id }}">{{ $_size->name }}</option>
                            @endforeach
                        </select>
                        @error('sizes_id') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="{{route('products.index')}}" class="btn btn-default">
                        Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        var photos = @json($product_photos);
        var valPhotos='';
        for (const photo of photos) {
            valPhotos += photo.url
        }
        $('#inputPhotos').html(valPhotos)

        var colors = @json($product_color);
        for (const color of colors) {
            $('#selectColors option[value=' + color.colors_id + ']').attr('selected', true);
        }

        var sizes = @json($product_size);
        for (const size of sizes) {
            $('#selectSizes option[value=' + size.sizes_id + ']').attr('selected', true);
        }

    </script>
@endpush
