@extends('adminlte::page')

@section('title', 'List Product')

@section('content_header')
    <h1 class="m-0 text-dark">List Product</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('products.create') }}" class="btn btn-primary mb-2">
                        Add New
                    </a>
                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Category</th>
                                <th>Name</th>
                                <th>Desc</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $key => $product)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$product->category}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->desc}}</td>
                                <td>
                                    <button type="button" class="btn btn-success btn-xs btnMdlPrices" data-id="{{ $product->id }}" data-product-name="{{ $product->name }}" data-size-price="{{ $product->size_price }}" data-toggle="modal" data-target="#mdlPrices">
                                        Prices
                                    </button>
                                    <button type="button" class="btn btn-success btn-xs btnMdlShow" data-id="{{ $product->id }}" data-product="{{ $product }}" data-toggle="modal" data-target="#mdlShow">
                                        Show
                                    </button>
                                    <a href="{{route('products.edit', $product)}}" class="btn btn-primary btn-xs">
                                        Edit
                                    </a>
                                    <a href="{{route('products.destroy', $product)}}" onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-xs">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Category</th>
                                <th>Name</th>
                                <th>Desc</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="mdlShow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlShowTitle" ></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="mdlShowBody">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="mdlPrices" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="mdlTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" id="formUpdatePrice">
                    @method('PUT')
                    @csrf
                    <input type="hidden" name="action" value="update_price">
                    <table class="table table-light">
                        <thead class="thead-light">
                            <tr>
                                <th>Size</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody id="tblPrices"></tbody>
                    </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btnSavePrice">Save changes</button>
            </div>
            </form>
        </div>
        </div>
    </div>
@stop

@push('js')
    <form action="" id="delete-form" method="post">
        @method('delete')
        @csrf
    </form>
    <script>
        $('#example2').DataTable({
            "responsive": true,
            "stateSave": true,
            initComplete: function () {
            this.api().columns(1).every( function () {
                var column = this;
                    var select = $('<br><select><option value=""></option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
            } );
        }
        });

        $('.btnMdlShow').on('click', function () {
            console.log($(this).data('product'))
            var product = $(this).data('product');
            $('#mdlShowTitle').text(product.name);
            var html=`
                <table class="table table-light table-bordered">
                    <tbody>
                        <tr>
                            <td>Category</td>
                            <td>:</td>
                            <td>${product.category}</td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>:</td>
                            <td>${product.name}</td>
                        </tr>
                        <tr>
                            <td>Desc</td>
                            <td>:</td>
                            <td>${product.desc}</td>
                        </tr>
                        <tr>
                            <td>Photo</td>
                            <td>:</td>
                            <td>${product.photos}</td>
                        </tr>
                        <tr>
                            <td>Colors</td>
                            <td>:</td>
                            <td>${product.colors}</td>
                        </tr>
                        <tr>
                            <td>Size</td>
                            <td>:</td>
                            <td>${product.sizes}</td>
                        </tr>
                    </tbody>
                </table>
            `;

            $('#mdlShowBody').html(html)

        })

        $('.btnMdlPrices').on('click',function() {
            $('#tblPrices').html('')
            $('#tblPrices').html('loading .....')
            $('#mdlTitle').html($(this).data('productName'))
            var html;
            var dt  = $(this).data('sizePrice');
            var product_id = $(this).data('id')
            var route = `products/${product_id}`
            console.log(route)
            $('#formUpdatePrice').attr('action',route)
            console.log(dt)
            for(const d of dt) {
                html+=`<tr>
                    <td>${d.size}</td>
                    <td>
                        <input type="hidden" name="size_price_id[]" value="${d.size_price_id}">
                        <input type="number" min="0" name="price[]" class="form-control" value="${d.price}" required>
                    </td>
                </tr>`
            }
            $('#tblPrices').html(html)
        })

        function notificationBeforeDelete(event, el) {
            event.preventDefault();
            if (confirm('Apakah anda yakin akan menghapus data ? ')) {
                $("#delete-form").attr('action', $(el).attr('href'));
                $("#delete-form").submit();
            }
        }

    </script>
@endpush
