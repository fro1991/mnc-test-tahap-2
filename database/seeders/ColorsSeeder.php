<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Colors;

class ColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Colors::truncate();
        Colors::insert([
            [
                'id' => 1,
                'name' => 'Merah'
            ],
            [
                'id' => 2,
                'name' => 'Biru'
            ],
            [
                'id' => 3,
                'name' => 'Hitam'
            ],
            [
                'id' => 4,
                'name' => 'Abu-abu'
            ],
        ]);
    }
}
