<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categories;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::truncate();
        Categories::insert([
            [
                'id' => 1,
                'name' => 'Pakaian'
            ],
            [
                'id' => 2,
                'name' => 'Celana'
            ],
            [
                'id' => 3,
                'name' => 'Topi'
            ],
            [
                'id' => 4,
                'name' => 'Jas'
            ],
            [
                'id' => 5,
                'name' => 'Sepatu'
            ],
            [
                'id' => 6,
                'name' => 'Jaket'
            ],
            [
                'id' => 7,
                'name' => 'Tas'
            ],
            [
                'id' => 8,
                'name' => 'Dompet'
            ],
        ]);
    }
}
