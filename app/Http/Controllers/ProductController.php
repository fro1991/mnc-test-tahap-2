<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Sizes;
use App\Models\Colors;
use App\Models\Categories;

use App\Models\Products;
use App\Models\ProductColors;
use App\Models\ProductPhotos;
use App\Models\ProductSizePrice;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Products::select(
                        DB::raw("
                            categories.name as category,
                            products.id,
                            products.name,
                            products.desc,
                            (select GROUP_CONCAT(url) from product_photos where products_id = products.id) as photos,
                            (select GROUP_CONCAT(colors.name) from product_colors left join colors on colors.id = product_colors.colors_id where products_id = products.id) as colors,
                            GROUP_CONCAT(sizes.name) as sizes,
                            CONCAT(
                                '[',
                                GROUP_CONCAT(
                                    JSON_OBJECT(
                                        'size_price_id',product_size_price.id,
                                        'size', sizes.name,
                                        'price', product_size_price.price
                                    )
                                ),
                                ']'
                            )
                            AS size_price
                        ")
                    )
                    ->join('categories','categories.id','=','products.categories_id')
                    ->join('product_size_price','product_size_price.products_id','=','products.id')
                    ->join('sizes','sizes.id','=','product_size_price.sizes_id')
                    ->groupBy('products.id','products.name','products.desc','categories.name')
                    ->get();
        return view('products.index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('products.create',[
            'categories' => Categories::all(),
            'colors' => Colors::all(),
            'sizes' => Sizes::all(),
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $product = New Products;
        $product->categories_id = $request->categories_id;
        $product->name = $request->name;
        $product->desc = $request->desc;
        $product->save();

        $product_id = $product->id;

        $photos = [];
        $request->photos = explode(PHP_EOL,$request->photos);
        foreach($request->photos as $_photo) {
            array_push($photos,['products_id' => $product_id, 'url' => $_photo]);
        }
        ProductPhotos::insert($photos);

        $colors = [];
        foreach($request->colors_id as $_col) {
            array_push($colors,['products_id' => $product_id, 'colors_id' => $_col]);
        }
        ProductColors::insert($colors);

        $sizes = [];
        foreach($request->sizes_id as $_siz) {
            array_push($sizes,['products_id' => $product_id, 'sizes_id' => $_siz]);
        }
        ProductSizePrice::insert($sizes);

        return redirect()->route('products.index')->with('success_message', 'New Product Added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Products::find($id);
        $product_photos = ProductPhotos::where('products_id','=',$id)->get();
        $product_size = ProductSizePrice::where('products_id','=',$id)->get();
        $product_color = ProductColors::where('products_id','=',$id)->get();

        return view('products.edit',[
            'categories' => Categories::all(),
            'colors' => Colors::all(),
            'sizes' => Sizes::all(),
            'product' => $product,
            'product_photos' => $product_photos,
            'product_size' => $product_size,
            'product_color' => $product_color,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product_id = $id;
        if($request->action == 'update_price') {
            foreach ($request->size_price_id as $key => $spi) {
                $size_price = ProductSizePrice::find($spi);
                $size_price->price = $request->price[$key];
                $size_price->save();
            }
        }
        elseif ($request->action == 'update_info') {
            $product = Products::find($product_id);
            $product->categories_id = $request->categories_id;
            $product->name = $request->name;
            $product->desc = $request->desc;
            $product->save();

            ProductPhotos::where('products_id','=',$product_id)->delete();
            $photos = [];
            $request->photos = explode(PHP_EOL,$request->photos);
            foreach($request->photos as $_photo) {
                array_push($photos,['products_id' => $product_id, 'url' => $_photo]);
            }
            ProductPhotos::insert($photos);

            ProductColors::where('products_id','=',$product_id)->delete();
            $colors = [];
            foreach($request->colors_id as $_col) {
                array_push($colors,['products_id' => $product_id, 'colors_id' => $_col]);
            }
            ProductColors::insert($colors);

            $curr_psp = ProductSizePrice::where('products_id','=',$product_id)->get();

            ProductSizePrice::where('products_id','=',$product_id)->delete();
            $sizes = [];
            foreach($request->sizes_id as $_siz) {
                array_push($sizes,['products_id' => $product_id, 'sizes_id' => $_siz]);
            }
            ProductSizePrice::insert($sizes);

            foreach($curr_psp as $psp) {
                ProductSizePrice::where('products_id','=',$product_id)
                                ->where('sizes_id','=',$psp->sizes_id)
                                ->update(['price' => $psp->price]);
            }
        }
        else {
            return redirect()->route('products.index')->with('error', 'Page Not Found');
        }
        return redirect()->route('products.index')->with('success_message', 'Product Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
